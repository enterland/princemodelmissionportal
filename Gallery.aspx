﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Gallery.aspx.cs" Inherits="PrinceModelMission.Gallery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <meta content="xenia - responsive and retina ready template" name="description">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport" />
    <link href="Content/GalleryContent/images/favicon.html" rel="shortcut icon" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="Content/GalleryContent/images/apple-touch-icon-144x144-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="Content/GalleryContent/images/apple-touch-icon-114x114-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="Content/GalleryContent/images/apple-touch-icon-72x72-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" href="Content/GalleryContent/images/apple-touch-icon-precomposed.png" />

    <!-- JS FILES -->
    <script type="text/javascript" src="Content/GalleryContent/js/jquery-1.20.2.min.js"></script>
    <script type="text/javascript" src="Content/GalleryContent/js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="Content/GalleryContent/js/modernizr.custom.js"></script>
    <!-- CSS FILES -->
    <link href="Content/GalleryContent/css/cubeportfolio-4.min.css" media="screen" rel="stylesheet" type="text/css">
    <link href="Content/GalleryContent/css/style.css" media="screen" rel="stylesheet" type="text/css">
    <%--<link href="Content/GalleryContent/css/responsive.css" media="screen" rel="stylesheet" type="text/css">--%>

    <div class="page-in">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 pull-left">
                    <div class="page-in-name">Gallery <span></span></div>
                </div>

            </div>
        </div>
    </div>
    <div class="container marg50">
        <div class="row">
            <div class="col-lg-12">
                <div id="filters-container-portfolio-4" class="cbp-l-filters-button">
                    <button data-filter="*" class="cbp-filter-item-active cbp-filter-item">
                        All<div class="cbp-filter-counter"></div>
                    </button>
                    <button data-filter=".wordpress" class="cbp-filter-item">
                        Images<div class="cbp-filter-counter"></div>
                    </button>
                    <button data-filter=".design" class="cbp-filter-item">
                        Videos<div class="cbp-filter-counter"></div>
                    </button>

                </div>
            </div>
        </div>
    </div>
    <div class="container marg50">
        <div class="grid hover-3">
            <div class="cbp-l-grid-projects" id="grid-container-portfolio-4">
                <ul>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>
                                <a href="InstitueImages/KMM New (1).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (1).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>
                                <a href="InstitueImages/KMM New (2).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (2).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>
                                <a href="InstitueImages/KMM New (3).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (3).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>
                                <a href="InstitueImages/KMM New (4).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (4).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>
                                <a href="InstitueImages/KMM New (5).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (5).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/KMM New (6).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (6).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/KMM New (7).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (7).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/KMM New (8).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (8).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/KMM New (9).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (9).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/KMM New (10).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (10).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/KMM New (11).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (11).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/KMM New (12).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (12).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/KMM New (13).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (13).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/KMM New (14).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (14).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/KMM New (15).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (15).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/KMM New (16).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (16).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/KMM New (17).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (17).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/KMM New (18).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (18).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/KMM New (19).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (19).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/KMM New (20).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (20).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>
                                <a href="InstitueImages/KMM New (21).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (21).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>
                                <a href="InstitueImages/KMM New (22).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (22).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>
                                <a href="InstitueImages/KMM New (23).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (23).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>
                                <a href="InstitueImages/KMM New (24).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (24).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/KMM New (25).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (25).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/KMM New (26).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (26).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/KMM New (27).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (27).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/KMM New (28).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (28).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/KMM New (29).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (29).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/KMM New (30).jpeg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/KMM New (30).jpeg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/Khadiza (2).jpg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/Khadiza (2).jpg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/Khadiza (3).jpg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/Khadiza (3).jpg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/Khadiza (4).jpg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/Khadiza (4).jpg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/Khadiza (5).jpg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/Khadiza (5).jpg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/Khadiza (6).jpg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/Khadiza (6).jpg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/Khadiza (7).jpg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/Khadiza (7).jpg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>
                                <a href="InstitueImages/Khadiza (8).jpg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/Khadiza (8).jpg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>
                                <a href="InstitueImages/Khadiza (9).jpg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/Khadiza (9).jpg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>
                                <a href="InstitueImages/Khadiza (10).jpg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/Khadiza (10).jpg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>
                                <a href="InstitueImages/Khadiza (11).jpg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/Khadiza (11).jpg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/Khadiza (12).jpg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/Khadiza (12).jpg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/Khadiza (13).jpg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/Khadiza (13).jpg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/Khadiza (14).jpg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/Khadiza (14).jpg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/Khadiza (15).jpg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/Khadiza (15).jpg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/Khadiza (16).jpg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/Khadiza (16).jpg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/Khadiza (17).jpg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/Khadiza (17).jpg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/Khadiza (18).jpg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/Khadiza (18).jpg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/Khadiza (19).jpg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/Khadiza (19).jpg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/Khadiza (20).jpg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/Khadiza (20).jpg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/Khadiza (21).jpg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/Khadiza (21).jpg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/Khadiza (22).jpg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/Khadiza (22).jpg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                    <li class="cbp-item wordpress html">
                        <div class="portfolio-main">
                            <figure>

                                <a href="InstitueImages/Khadiza (23).jpg" class="portfolio-attach cbp-lightbox" data-title="">
                                    <img src="InstitueImages/Khadiza (23).jpg" alt=""></a>

                            </figure>
                        </div>
                    </li>
                </ul>
            </div>

        </div>
    </div>

    <script type="text/javascript" src="Content/GalleryContent/js/jquery.cubeportfolio.min.js"></script>
    <script type="text/javascript" src="Content/GalleryContent/js/portfolio-4.js"></script>

    <%--<script type="text/javascript" src="Content/GalleryContent/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="Content/GalleryContent/js/waypoints.min.js"></script>
    <script type="text/javascript" src="Content/GalleryContent/js/sticky.js"></script>
    <script type="text/javascript" src="Content/GalleryContent/js/jquery.tweet.min.js"></script>
    <script type="text/javascript" src="Content/GalleryContent/js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="Content/GalleryContent/js/retina.js"></script>
    <script type="text/javascript" src="Content/GalleryContent/js/jquery.cubeportfolio.min.js"></script>
    <script type="text/javascript" src="Content/GalleryContent/js/portfolio-4.js"></script>
    <script type="text/javascript" src="Content/GalleryContent/js/jquery.dlmenu.js"></script>
    <script type="text/javascript" src="Content/GalleryContent/js/main.js"></script>--%>
</asp:Content>
