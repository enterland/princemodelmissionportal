﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="PrinceModelMission.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2F100014616614359&tabs=timeline&width=0&height=0&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=352210228765660" width="0" height="0" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
    <div class="article-type style1">
        <p class="article-type style1">
            Contact Us
                       
        </p>
        <hr />
    </div>
    <h3>Prince Model Mission</h3>
    <div class="  col-md-4 col-sm-12">
        <div class="promo-block">
            <div class="promo-text">Information</div>
            <div class="center-line"></div>
        </div>
        <div class="marg50">
            <ul class="contact-footer">
                <li><strong><i class="icon-location"></i>Address:</strong> Rejianagar - back side of Rejinagar Police Station, P.O.- Rejianagar, P.S.- Rejianagar, Dist.- Murshidabad (WEST BENGAL), 742189</li>
                <li><strong><i class="icon-mobile"></i>Phone:</strong> +91 91534 75308 / +91 70018 73576</li>
                <li><strong><i class="icon-mail"></i>E-mail:</strong> princemodelmission@gmail.com</li>
                <li><strong><i class="icon-key"></i>Time:</strong> from 6:00 am to 6:00 pm</li>
            </ul>
            <hr>
        </div>
    </div>
    , 

    <div class="page-in">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 pull-left">
                    <%--<div class="page-in-name">Contact <span>US</span></div>--%>
                </div>

            </div>
        </div>
    </div>
    <div class="mapouter">
        <div class="gmap_canvas">
            <iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=prince%20model%20mission%20rejinagar&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
            <a href="https://www.embedgooglemap.net/blog/divi-discount-code-elegant-themes-coupon/">divi discount</a><br>
            <style>
                .mapouter {
                    position: relative;
                    text-align: right;
                    height: 500px;
                    width: 600px;
                }
            </style>
            <a href="https://www.embedgooglemap.net">google map html widget</a><style>
                                                                                   .gmap_canvas {
                                                                                       overflow: hidden;
                                                                                       background: none !important;
                                                                                       height: 500px;
                                                                                       width: 600px;
                                                                                   }
                                                                               </style></div>
    </div>
    <hr>


    <%--<h3>Your contact page.</h3>
    <address>
        One Microsoft Way<br />
        Redmond, WA 98052-6399<br />
        <abbr title="Phone">P:</abbr>
        425.555.0100
    </address>

    <address>
        <strong>Support:</strong>   <a href="mailto:Support@example.com">Support@example.com</a><br />
        <strong>Marketing:</strong> <a href="mailto:Marketing@example.com">Marketing@example.com</a>
    </address>--%>
</asp:Content>
